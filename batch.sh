#!/bin/bash
# Borja Docampo Alvarez, 2017
#SBATCH -N 1
#SBATCH -n 16
#SBATCH -p sun
#SBATCH -J NPT_EMIM
#SBATCH -e batch.out
#SBATCH -o batch.out
#SBATCH -x SUN010,SUN011,SUN012,SUN013
###################
# Packing and box setup
packmol < box.inp
gmx editconf -f boxed.pdb -o boxed.gro -box 4.4 4.4 10.0 -center 2.2 2.2 5.0
# Steepest descent minimization
gmx grompp -f NPT_steep.mdp -p topology.top -c boxed.gro -o NPT_steep.tpr
gmx mdrun -nt 16 -pin on -v -deffnm NPT_steep
# Conjugate gradients minimization
gmx grompp -f NPT_cg.mdp -p topology.top -c NPT_steep.gro -o NPT_cg.tpr
gmx mdrun -nt 16 -pin on -v -deffnm NPT_cg
# Short XY NPT stabilization
gmx grompp -f NPT_xy.mdp -p topology.top -c NPT_cg.gro -o NPT_xy.tpr
gmx mdrun -nt 16 -pin on -v -deffnm NPT_xy
# 5-point anneal
gmx grompp -f NPT_anneal.mdp -p topology.top -c NPT_xy.gro -o NPT_anneal.tpr
gmx mdrun -nt 16 -pin on -v -deffnm NPT_anneal
# Stabilization
gmx grompp -f NPT_stab.mdp -p topology.top -c NPT_anneal.gro -o NPT_stab.tpr
gmx mdrun -nt 16 -pin on -v -deffnm NPT_stab
# Production run
gmx grompp -f NPT_run.mdp -p topology.top -c NPT_stab.gro -o NPT_run.tpr
gmx mdrun -nt 16 -pin on -v -deffnm NPT_run
